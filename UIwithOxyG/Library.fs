﻿namespace UIwithOxyG
module Library =

    open System

    let rnd = Random()
    let makeData n =  (Array.init n (fun _ -> rnd.NextDouble()-0.5) |> Array.scan (+) 0.)

