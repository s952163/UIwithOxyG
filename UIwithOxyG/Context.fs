﻿namespace UIwithOxyG
module Context =


    open System
    open System.Windows
    open Gjallarhorn
    open Gjallarhorn.Bindable
    open Library
    open OxyPlot
    open OxyPlot.Wpf

    type OxyChart(xs,title) =
        member __.Points = xs
        member __.Title = title


    let create () =
        /// Create our binding source
        let source = Binding.createSource ()
        
        /// Wrap data into DataPoint
        let dpWrap x = x |> Array.mapi (fun i x -> (DataPoint(float i,x)))
        let pts' = Library.makeData 365 |> dpWrap
        let chart = OxyChart(pts',"Hello Gjallarhorn!")
        //let chartX = chart |> Mutable.create
        let pts = chart.Points  |> Mutable.create
        let date2 = DateTime.Today |> Mutable.create 
        let date1' = Signal.map (fun (x:DateTime) -> x.AddDays(-365.)) date2
        let date1 = Mutable.create date1'.Value
        //Signal.Subscription.copyTo date1 date1' |> source.AddDisposable
        let dateRange = Signal.map2 (fun (d1:DateTime) (d2:DateTime) -> (d2 - d1).Days) date1 date2
        //create another data series
        let xs2 = Library.makeData dateRange.Value |> dpWrap |> Mutable.create
        // Bind the Series
        Binding.toView source "Series1" pts
        Binding.toView source "Title" (chart.Title |> Mutable.create)
        // Add another series
        Binding.toView source "Series2" xs2
        Binding.mutateToFromView source "Date2" date2
        Binding.mutateToFromView source "Date1" date1
        
        /// update the data when this function is called
        let makeXs (pts:IMutatable<_>) n  =
            pts.Value <- Library.makeData n |> dpWrap
            xs2.Value <- Library.makeData n |> dpWrap
        
        Signal.Subscription.create(fun x -> makeXs pts x) dateRange |> source.AddDisposable
        // Bind a command to a button
        Binding.createCommand "runCmd" source
        |> Observable.subscribe (fun _ -> makeXs pts dateRange.Value)
        |> source.AddDisposable
    
        source

